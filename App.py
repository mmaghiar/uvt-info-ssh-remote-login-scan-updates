#!/Users/maghiar.mihai/storage/work/personal/projects/py_gen_env/bin/python

import logging, sys, nmap, argparse, paramiko

HOST_USER = "root"
HOST_PASS = ""

class Input():

    def args(self):
        pass

    def inputArgs(self):
        parser = argparse.ArgumentParser()
        parser.add_argument("-c","--create", dest="create", action="store", type=str, help="")
        parser.add_argument("-v","--verbose", dest="verbose", action="store", type=int, help="Program output")
        args = parser.parse_args()
        try:
            if len(self.args.create) > 0:
                print self.input.args.create
                self.installPath = self.input.args.create
                deploy = ""
                deploy.create(self.installPath)

            else:
                self.log.info("No arguments supplied. Exiting program ...")
                sys.exit()
        except:
            pass
        return args

class Output():
    def printHost(host, scan_result):
        print '------------------'
        print host, scan_result

class Authentication():
    def basic(self):
        pass

class Host():

    def connect(self, host, user, passwd):
        logging.getLogger("paramiko").setLevel(logging.WARNING)
        conn = paramiko.SSHClient()
        conn.set_missing_host_key_policy(paramiko.AutoAddPolicy())
        conn.load_system_host_keys()
        conn.connect(host, username=user, password=passwd, timeout=10)
        conn.close()

        if conn is not None:
            print "Connecting to " + host + " is succesfull."
        else:
            print "Connecting to " + host + " failed."

        '''try:
            #pass
            stdin, stdout, stderr = ssh.exec_command("ls -l \n")
        except:
            sys.exit()
        print stdout.readlines()'''

    def scan(self, host='', port='', arguments=''):
        nm = nmap.PortScanner()
        nm.scan(host, port)
        return nm.all_hosts()
        #for host in nm.all_hosts():
        #    print nm.all_hosts()
        #    print('----------------------------------------------------')
        #    print('Host : %s (%s)' % (host, nm[host].hostname()))
        #    print('State : %s' % nm[host].state())


class AppController():
    def __init__(self, Input, Output):
        self.input = Input
        self.output = Output

    def run(self):
        checkHost = Host()
        hostList = checkHost.scan('194.102.63.128/26', '22')
        print hostList
        for host in hostList:
            print host
            try:
                checkHost.connect(host, HOST_USER, HOST_PASS)
            except:
                print "next host"

def main():
    log = logging.getLogger("ServerCheck")
    log.info("Application start ...")
    print logging.basicConfig(stream=sys.stdout, level=logging.DEBUG)
    input = Input()
    output = Output()
    app = AppController(input, output)
    app.run()

if __name__ == '__main__':
    main()



